"""
useful for creating filenames from paths
"""

from nipype.interfaces.utility import Function


def replace_slash_fn(filename):
    renamed = "_".join(str(filename).split("/"))
    return renamed


replace_slash = Function(input_names=["filename"],
                         output_names=["renamed"],
                         function=replace_slash_fn)
