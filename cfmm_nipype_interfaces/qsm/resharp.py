"""
Cornell implementation of resharp, executed in matlab
"""

import traits.api as traits
import os
from nipype.interfaces.base import CommandLineInputSpec, TraitedSpec, File
from ..matlab_runmode import MatlabRunMode
from ..MatlabBaseInterface import MatlabBaseInterface
from . import matlab_scripts


class RESHARPInputSpec(CommandLineInputSpec):
    freq_loc = File(exists=True, desc='Input filename', argstr='%s', copyfile=False, mandatory=True)
    mask_loc = File(exists=True, desc='Mask (3D)', argstr='%s', copyfile=False, mandatory=True)
    radius = traits.Float(5.0, desc='the radius of the spherical mean value operation in mm', argstr='%s',
                          mandatory=False, usedefault=True)
    alpha = traits.Float(0.05, desc='the regularizaiton parameter used in Tikhonov', argstr='%s', mandatory=False,
                         usedefault=True)
    LFS_filename = File(desc="Output LFS filename", argstr='%s', mandatory=True)


class RESHARPOutputSpec(TraitedSpec):
    LFS_filename = File(desc="Local frequency shift with background removed",
                        exists=True)
    LFS_mask_filename = File(desc="Local frequency shift with background removed",
                             exists=True)


class RESHARP(MatlabBaseInterface):
    input_spec = RESHARPInputSpec
    output_spec = RESHARPOutputSpec

    def __init__(self, run_mode=MatlabRunMode.CONTAINER_MCR,
                 matlab_executable='/usr/local/matlab/R2016b/bin/matlab',
                 mcr_loc='/opt/mcr/v91', matlab_execution_override=None):
        matlab_scripts_dir = matlab_scripts.__path__._path[0]
        matlab_script_name = 'RESHARP_matlabscript'
        super().__init__(matlab_scripts_dir, matlab_script_name, run_mode, matlab_executable, mcr_loc,
                         matlab_execution_override)

    def _run_interface(self, runtime):
        matlab_execution = self.command

        cmd = matlab_execution.replace('<params>', ' %s %s %s %s %s' % (
            self.inputs.freq_loc,
            self.inputs.mask_loc,
            self.inputs.radius,
            self.inputs.alpha,
            self.inputs.LFS_filename
        ))
        os.system(cmd)
        return runtime

    def _list_outputs(self):
        outputs = self.output_spec().get()
        outputs['LFS_filename'] = os.path.abspath(self.inputs.LFS_filename)
        LFS_mask_filename = outputs['LFS_filename']
        LFS_mask_filename,ext = LFS_mask_filename.split('.nii')
        LFS_mask_filename = LFS_mask_filename + "_mask.nii" + ext
        outputs['LFS_mask_filename'] = os.path.abspath(LFS_mask_filename)
        return outputs
