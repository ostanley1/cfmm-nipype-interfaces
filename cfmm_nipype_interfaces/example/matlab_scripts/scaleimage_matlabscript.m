function [ output_image ] = scaleimage_matlabscript( input_filename, scaling, output_filename )
    % to compile this function for MCR execution use the command:
    % mcc -mv -I <path to>/NIfTI_20140122 scaleimage.m
    % this creates the compiled code in scaleimag and also creates 
    % the run script run_scaleimage.sh. Use the script as follows:
    % ./run_scaleimage.sh <path to MCR instalation> <fn parameters>
    
    % the isdeployed flag allows us to use addpath only during matlab execution
    % and not when compiling the code for MCR (which throws errors)
    % https://www.mathworks.com/matlabcentral/answers/102228-why-does-addpath-cause-an-application-that-is-generated-with-matlab-compiler-4-0-r14-to-fail
    if ~isdeployed
        %we use the NIfTI_20140122 library to read and save nifti files
        addpath([fileparts(mfilename('fullpath')),'/NIfTI_20140122']);
    end

    % get input images and values
    input_image_obj = load_nii(input_filename);
    input_image = double(input_image_obj.img);
    % When the function is called from the command line, the inputs come as
    % strings and we need to convert them.
    if isstring(scaling) || ischar(scaling)
        scaling = str2double(scaling);
    end
    
    % process inputs
    output_image = input_image * scaling;
    
    % save output files    
    output_image_obj = make_nii(output_image);
    % same FOV and orientation as input, so just use the same header
    output_image_obj.hdr = input_image_obj.hdr;    
    save_nii(output_image_obj,output_filename);
    
    if false
        % this is how I test functions I'm writing in matlab
        % I would remove this from the final version of the code.
        % Note the test code is saved as part of the function but is never
        % executed because of the if false statement. Without the if false
        % statement the code will enter into an infinite recursion.
        % I use this code to test the function by selecting what's inside 
        % the if statement and pressing F9 to execute the selection
        addpath('NIfTI_20140122');
        test_input_img_name = 'test.nii.gz';
        test_output_img_name = 'out.nii.gz';
        test_input_img = ones(2);
        test_input_img_obj = make_nii(test_input_img);
        save_nii(test_input_img_obj,test_input_img_name);

        scaleimage_matlabscript(test_input_img_name, 2.0, test_output_img_name)

        output_img_obj = load_nii(test_output_img_name);
        output_img = double(output_img_obj.img);
        % this should display 2s
        disp(output_img);
        %cleanup
        delete(test_input_img_name, test_output_img_name);
    end
end