"""
A simple example interface for executing matlab code in a pipeline.
"""

from nipype.interfaces.base import BaseInterface, CommandLineInputSpec, TraitedSpec, File
import traits.api as traits
import os
from cfmm_nipype_interfaces import MatlabRunMode
from ..MatlabBaseInterface import MatlabBaseInterface
# we import that matlab_scripts folder to get it's absolute path. This will be used for creating the default matlab
# command for the ScaleImage interface
import cfmm_nipype_interfaces.example.matlab_scripts as matlab_scripts

class ScaleImageInputSpec(CommandLineInputSpec):
    # Inputs to the node. Here we can designate whether or not an error should be thrown if the input file does not
    # exist. We can also designate which inputs are mandatory and if there should be a default value.
    input_filename = File(exists=True, desc='Input image to scale.', copyFile=False, mandatory=True)
    scaling_factor = traits.Float(1.0, desc='Scaling factor.', mandatory=False, usedefault=True)
    output_filename = File(desc="Name of output file", mandatory=True)


class ScaleImageOutputSpec(TraitedSpec):
    # Tell the node the name of your output objects. You will define the file location of the outputs in _list_outputs()
    # The node will check to make sure your _run_interface() created the designated outputs in the correct location
    output_scaled = File(desc="Path to the saved output image.", exists=True)


class ScaleImage(MatlabBaseInterface):
    input_spec = ScaleImageInputSpec
    output_spec = ScaleImageOutputSpec

    def __init__(self, run_mode=MatlabRunMode.CONTAINER_MCR,
                 matlab_executable='/usr/local/matlab/R2016b/bin/matlab',
                 mcr_loc='/opt/mcr/v91', matlab_execution_override=None):
        # matlab scripts can be run with the matlab executable or mcr. run_mode describes in what mode the node
        # should be run. matlab_executable and mcr_loc will be used by the parent class MatlabBaseInterface
        # to fashion a matlab command based on the run_mode.
        # if run_mode=MatlabRunMode.LOCAL_MATLAB, use matlab_executable to call matlab and execute code
        # if run_mode=MatlabRunMode.LOCAL_MCR, use MCR installed in location mcr_loc to execute code
        # if run_mode=MatlabRunMode.CONTAINER_MCR, use MCR installed in MatlabBaseInterface.container_mcr_loc to
        #  execute code
        matlab_scripts_dir = matlab_scripts.__path__._path[0]
        matlab_script_name = 'scaleimage_matlabscript'
        super().__init__(matlab_scripts_dir, matlab_script_name, run_mode, matlab_executable, mcr_loc,
                         matlab_execution_override)

    def _run_interface(self, runtime):
        #self.command was created by the parent class MatlabBaseInterface during initialization
        matlab_execution = self.command

        # get input filenames and values
        input_filename = self.inputs.input_filename
        scaling = self.inputs.scaling_factor
        output_filename = self.inputs.output_filename

        # process inputs through matlab
        # matlab will create the output files
        cmd = matlab_execution.replace('<params>', ' %s %s %s' % (
            input_filename,
            scaling,
            output_filename
        ))
        os.system(cmd)
        return runtime

    def _list_outputs(self):
        # Define the file location of your output objects.
        # This helps the node enforce checks to make sure your _run_interface() actually does what you say it will do.
        # ie. The node will check you created the designated output files in the correct location.
        outputs = self.output_spec().get()
        outputs['output_scaled'] = os.path.abspath(self.inputs.output_filename)
        return outputs
