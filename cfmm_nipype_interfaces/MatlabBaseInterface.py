from nipype.interfaces.base import BaseInterface
from .matlab_runmode import MatlabRunMode
import os


class MatlabBaseInterface(BaseInterface):
    container_mcr_loc = '/opt/mcr/v91'

    def __init__(self, matlab_scripts_dir, matlab_script_name, run_mode=MatlabRunMode.CONTAINER_MCR,
                 matlab_executable='/usr/local/matlab/R2016b/bin/matlab',
                 mcr_loc='/opt/mcr/v91', matlab_execution_override=None):
        # matlab scripts can be run with the matlab executable or mcr. run_mode describes in what mode the node
        # should be run. All other attribute's are used to fashion _run_interface()'s matlab_execution variable
        # in different run scenarios.
        # if run_mode=MatlabRunMode.LOCAL_MATLAB, use matlab_executable to call matlab and execute code
        # if run_mode=MatlabRunMode.LOCAL_MCR, use MCR installed in location mcr_loc to execute code
        # if run_mode=MatlabRunMode.CONTAINER_MCR, use MCR installed in MatlabBaseInterface.container_mcr_loc to
        #  execute code
        self.run_mode = run_mode
        self.matlab_loc = matlab_executable
        self.mcr_loc = mcr_loc
        self.matlab_scripts_dir = matlab_scripts_dir
        self.matlab_script_name = matlab_script_name
        self.matlab_execution_override = matlab_execution_override
        self.set_matlab_execution()
        super().__init__()

    def set_matlab_execution(self):
        # This function crafts the correct command for the interface. ie. If the test class is
        # instatiated with local matlab, we craft an appropriate command for calling the executable, if it's
        # MCR, the command changes accordingly. Note the <params> substring will be replaced with the interface's
        # input variables.

        if self.matlab_execution_override:
            matlab_execution = self.matlab_execution_override
        else:
            if self.run_mode == MatlabRunMode.LOCAL_MATLAB:
                matlab_execution = self.matlab_loc + ' -nodisplay -nosplash -r "addpath ' + os.path.abspath(
                    self.matlab_scripts_dir) + ';' + self.matlab_script_name + ' <params>;quit;"'
            elif self.run_mode == MatlabRunMode.LOCAL_MCR:
                matlab_execution = os.path.join(self.matlab_scripts_dir,
                                                'run_' + self.matlab_script_name + '.sh') + ' ' + self.mcr_loc + ' <params>'
            elif self.run_mode == MatlabRunMode.CONTAINER_MCR:
                matlab_execution = os.path.join(self.matlab_scripts_dir,
                                                'run_' + self.matlab_script_name + '.sh') + ' ' + self.container_mcr_loc + ' <params>'
        self.command = matlab_execution
