import numpy as np
from pyqsm.dipole_kernel import dipole_kernel
from pyqsm.dipole_convolve import dipole_convolve
import nibabel as nib
import json
import os
from .common import create_ellipsoid
from enum import Enum


class phantom_type(Enum):
    COMPLEX = 1
    FREQ = 2
    BIDS = 3


def create_phantom(dim_sz=100, FOV_mm=320, ptype=phantom_type.BIDS, bids_dir=None, siemens_phase=True, snr=False):
    radx, rady, radz = FOV_mm * .25, FOV_mm * .375, FOV_mm * .375  # mm
    main_ellipsoid = create_ellipsoid(FOV_mm, (radx, rady, radz), dim_sz)
    small_ellipsoid_1 = create_ellipsoid(FOV_mm, (.1 * radx, .2 * rady, .2 * radz), dim_sz, center=[radx * .65, 0, 0],
                                         angle=np.pi / 4, axes=[0, 0, 1])
    small_ellipsoid_2 = create_ellipsoid(FOV_mm, (.1 * radx, .2 * rady, .2 * radz), dim_sz,
                                         center=[-radx * .3, -rady * .3, -radz * .3], angle=np.pi / 4, axes=[1, 0, 1])
    small_ellipsoid_3 = create_ellipsoid(FOV_mm, (.1 * radx, .2 * rady, .2 * radz), dim_sz,
                                         center=[-0.5 * radx, .2 * rady, 0], angle=3 * np.pi / 2, axes=[1, 0, 1])
    mask = main_ellipsoid

    ellipsoids = np.stack((main_ellipsoid, small_ellipsoid_1, small_ellipsoid_2, small_ellipsoid_3), axis=-1)

    mag_values = np.asarray([1000, 200, -300, -500])
    mag = np.sum(ellipsoids * mag_values[np.newaxis, np.newaxis, np.newaxis, :], axis=-1)

    susc_values_ppb = np.asarray([0.0, -10, 70, 30])  # multiply by 1e-9 to get true value
    susc_ppb = np.sum(ellipsoids * susc_values_ppb[np.newaxis, np.newaxis, np.newaxis, :], axis=-1)
    susc = np.sum(ellipsoids * susc_values_ppb[np.newaxis, np.newaxis, np.newaxis, :] * 1e-9, axis=-1)

    B0 = 7  # Tesla
    larmour = 267.5221900 * 1e6  # rad/(s*T)
    CF = larmour * B0  # rad/s

    dipole_f_domain = dipole_kernel((dim_sz,) * 3, (FOV_mm,) * 3)

    # long calculation, more clear
    B_local_relative = dipole_convolve(susc, dipole_f_domain)
    B_local = (B_local_relative * B0) + B0  # Tesla
    B_local_baseband = B_local - B0  # Tesla
    freq_local_baseband = larmour * B_local_baseband  # rad/s

    # short calculation, more accurate
    # B_local_relative_ppb = dipole_convolve(susc_ppb,dipole_f_domain)
    # freq_local_baseband = B_local_relative_ppb * (CF*1e-9) # rad/s

    background_freq = np.linspace(150, 300, dim_sz)  # rad/s
    fieldmap = freq_local_baseband + background_freq[..., np.newaxis, np.newaxis]

    if ptype == phantom_type.FREQ:
        return fieldmap, ellipsoids, susc_values_ppb, freq_local_baseband

    r2star_values = np.asarray([50.0, 3, -5, 0])
    r2star = np.sum(ellipsoids * r2star_values[np.newaxis, np.newaxis, np.newaxis, :], axis=-1)

    n_time_samples = 5
    # sample between 5 and 20 ms
    TE = np.linspace(.005, .02, n_time_samples)  # s

    input_data = (mask * mag)[..., np.newaxis] * np.exp(
        -(r2star[..., np.newaxis] * TE) + 1j * fieldmap[..., np.newaxis] * TE)
    if snr:
        stdv = mag_values[0] / snr
        noise = stdv * np.random.randn(*input_data.shape) + 1j * stdv * np.random.randn(*input_data.shape)
        input_data = input_data + noise

    if ptype == phantom_type.COMPLEX:
        return input_data, ellipsoids, susc_values_ppb, r2star_values, freq_local_baseband, fieldmap, susc

    # file locations
    sub_name = 'sub-FAKE'
    if bids_dir == None:
        bids_dir = 'bids'
    generated_images_dir = os.path.join(bids_dir, sub_name, 'anat')
    os.makedirs(generated_images_dir, exist_ok=True)  # overwrite existing files
    # should create dataset_description.json if it doesn't exist, but we'll just ignore the BIDs warnings

    mag_files = [os.path.join(generated_images_dir, sub_name + '_part-mag_echo-' + str(n + 1) + '_GRE.nii.gz') for n in
                 range(n_time_samples)]
    phase_files = [os.path.join(generated_images_dir, sub_name + '_part-phase_echo-' + str(n + 1) + '_GRE.nii.gz') for n
                   in range(n_time_samples)]
    json_mag_files = [os.path.join(generated_images_dir, sub_name + '_part-mag_echo-' + str(n + 1) + '_GRE.json') for n
                      in range(n_time_samples)]
    json_phase_files = [os.path.join(generated_images_dir, sub_name + '_part-phase_echo-' + str(n + 1) + '_GRE.json')
                        for n in range(n_time_samples)]

    mag_data = np.abs(input_data)
    phase_data = np.angle(input_data)

    if siemens_phase:
        phase_data = (phase_data / np.pi + 1) * 2048

    for n in range(n_time_samples):
        jsonloc = json_mag_files[n]
        with open(jsonloc, 'w') as f:
            json.dump({"EchoTime": TE[n], "ImagingFrequency": CF / (2 * np.pi) * 1e-6}, f)  # bids CF in MHz

        jsonloc = json_phase_files[n]
        with open(jsonloc, 'w') as f:
            json.dump({"EchoTime": TE[n], "ImagingFrequency": CF / (2 * np.pi) * 1e-6}, f)  # bids CF in MHz

        img = nib.Nifti1Image(mag_data[..., n],
                              np.eye(4) * np.diag([FOV_mm / dim_sz, FOV_mm / dim_sz, FOV_mm / dim_sz, 1]))
        img.header.set_xyzt_units('mm')
        nib.save(img, mag_files[n])

        img = nib.Nifti1Image(phase_data[..., n],
                              np.eye(4) * np.diag([FOV_mm / dim_sz, FOV_mm / dim_sz, FOV_mm / dim_sz, 1]))
        img.header.set_xyzt_units('mm')
        nib.save(img, phase_files[n])
    if ptype == phantom_type.BIDS:
        return generated_images_dir, ellipsoids, susc_values_ppb, r2star_values, freq_local_baseband, fieldmap
