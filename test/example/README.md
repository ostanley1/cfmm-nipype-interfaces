These are the unittest files for the example interfaces.  Notice that the cfmm-nipype-interfaces/test diretory structure mimics cfmm-nipype-interfaces/cfmm_nipype_interfaces. The reason is that one unittest file should be included for every custom interface that you make. The unittest ensures that your interface continues to work as intended even after you change or make updates to it. This is accomplished through gitlab's continuous integration which runs these tests each time a commit is pushed to gitlab.com's repository. Each test directory should include a \_\_init\_\_.py file in order for its tests to be found during continuous integration. If your unittest fails, you will be alerted indicating that one of your changes has introduced a bug. 

### Running the tests with local python
To run the test with your local python (ideally with virtualenv), run the file test_scaleimage_matlab.py either in pycharm or from the command line. The code:
```
if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(TestScaleImage('test_scale_interface', run_mode=MatlabRunMode.LOCAL_MCR))
    runner = unittest.TextTestRunner()
    runner.run(suite)
```
at the end of the file will set up and run the test.  

You can switch the run_mode to use either MCR or MATLAB. If using `MatlabRunMode.LOCAL_MATLAB`, search for the variable `matlab_executable` and set it to the path of your local matlab executable. If using `MatlabRunMode.LOCAL_MCR`, search for the variable `mcr_location_local` and set it ot the path of your local MCR installation. You should leave the `mcr_location_in_container` path alone as it is only used by the continuous integration container.

Also note that unittest's discover feature will fail locally for matlab nodes (ie. if you try using `python -m unittest discover` from the command line). This is because tests for matlab interfaces require the `run_mode` variable to be set to `MatlabRunMode.LOCAL_MATLAB` or `MatlabRunMode.LOCAL_MCR` in order to be run locally.  However, the default value is `MatlabRunMode.CONTAINER_MCR` and variables cannot be passed to the test to override this behaviour when using unittest's discover.  Therefore local testing of matlab interfaces should be done on a per file basis as described previously.


### Running the tests with a local container
These tests will only work if you are on a unix computer and have gitlab-runner installed.
To test if your commit will pass the continuous integration tests on gitlab.com, you can first check the tests locally:
```
gitlab-runner exec docker --cache-dir /cache --env CI_REGISTRY_IMAGE=registry.gitlab.com/kuurstra/cfmm-nipype-interfaces --env CI_COMMIT_REF_SLUG=latest --docker-volumes `pwd`/ci:/cache test-python3
```
If the test fails, you might want to fix the bug and use `git commit --amend`

### Running the tests on gitlab.com with continuous integration
gitlab will automatically run every test whenever you push a commit to the remote repository. The continuous integration is configured in `cfmm-nipyp-interfaces/.gitlab-ci.yml`.
