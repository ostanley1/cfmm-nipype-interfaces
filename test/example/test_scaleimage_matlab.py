import nipype.pipeline.engine as pe
import unittest
from cfmm_nipype_interfaces.example.scaleimage_matlab import ScaleImage
import numpy.testing as npt
from cfmm_nipype_interfaces import MatlabRunMode
import numpy as np
import nibabel as nib
import tempfile
import os


class TestScaleImage(unittest.TestCase):
    # We add a class attribute designating if we want the test to run the node using 1) the path to our local matlab,
    # 2) the path to our local MCR, or 3) the container's path to MCR. We will use this attribute to help us craft
    # the interfaces's matlab_execution attribute. The class defaults to setup tests to run in a container. However,
    # in the if __name__=='__main__' section we override this to run the tests locally.
    def __init__(self, *args, run_mode=MatlabRunMode.CONTAINER_MCR, **kwargs):
        self.run_mode = run_mode
        super(TestScaleImage, self).__init__(*args, **kwargs)

    def setUp(self):
        # anything that all tests will need
        self.input_img = np.ones((3, 3, 3))
        affine = np.eye(4)
        input_img_nifti_obj = nib.Nifti1Pair(self.input_img, affine)
        self.tmpdir_obj = tempfile.TemporaryDirectory()
        self.tmpdir = self.tmpdir_obj.name
        self.test_input_image = os.path.join(self.tmpdir, 'test_input.nii.gz')
        nib.save(input_img_nifti_obj, self.test_input_image)

    def test_scale_interface(self):
        matlab_executable = '/usr/local/matlab/R2016b/bin/matlab'
        local_mcr_location = '/storage/akuurstr/MATLAB_R2016b/v91/'

        test = pe.Node(interface=ScaleImage(run_mode=self.run_mode, matlab_executable=matlab_executable,
                                            mcr_loc=local_mcr_location), name="test_ScaleImage")
        test.inputs.input_filename = self.test_input_image
        test.inputs.scaling_factor = 2.0
        test.inputs.output_filename = os.path.join(self.tmpdir, "my_scaled_image.nii.gz")

        test.run()

        node_result = nib.load(test.inputs.output_filename).get_data()
        expected_result = self.input_img * test.inputs.scaling_factor
        npt.assert_almost_equal(expected_result, node_result)

    def tearDown(self):
        # delete test files?
        # because we used a temproary directory, there's no need to manually delete files created during testing
        pass


if __name__ == '__main__':
    # if using pycharm, make sure your run configuration is setup to execute this as a script and not in pycharm's
    # built-in unittest environment.

    # we cannot our run_mode attribute with unittest's test discovery.
    # we must setup our testsuite manually and execute it.
    suite = unittest.TestSuite()
    # suite.addTest(TestScaleImage('test_scale_interface', run_mode=MatlabRunMode.LOCAL_MATLAB))
    suite.addTest(TestScaleImage('test_scale_interface', run_mode=MatlabRunMode.LOCAL_MCR))
    runner = unittest.TextTestRunner()
    runner.run(suite)
