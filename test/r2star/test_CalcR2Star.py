import nipype.pipeline.engine as pe
from numpy.random import randn
import os
import unittest
import json
from cfmm_nipype_interfaces.r2star import CalcR2Star, model_monoexponential_R2Star
import numpy as np
import nibabel as nib
import numpy.testing as npt
import tempfile


class TestCalcR2Star(unittest.TestCase):
    def setUp(self):
        self.n_spatial_samples = 2
        self.n_time_samples = 5

        # temporary file locations
        self.tmp_generated_images_dir_obj = tempfile.TemporaryDirectory()
        tmp_generated_images_dir = self.tmp_generated_images_dir_obj.name
        self.tmp_output_dir_obj = tempfile.TemporaryDirectory()
        tmp_output_dir = self.tmp_output_dir_obj.name

        self.mag = [os.path.join(tmp_generated_images_dir, 'mag_' + str(n + 1) + '.nii.gz') for n in
                    range(self.n_time_samples)]
        self.phase = [os.path.join(tmp_generated_images_dir, 'phase_' + str(n + 1) + '.nii.gz') for n in
                      range(self.n_time_samples)]
        self.json_file = [os.path.join(tmp_generated_images_dir, 'json_' + str(n + 1)) for n in
                          range(self.n_time_samples)]
        self.freq_loc = os.path.join(tmp_generated_images_dir, 'freq.nii.gz')
        self.mask = os.path.join(tmp_generated_images_dir, 'mask.nii.gz')

        self.R2star = os.path.join(tmp_output_dir, 'R2star_estimated.nii.gz')
        self.neg_mask = os.path.join(tmp_output_dir, 'neg_mask.nii.gz')
        self.nan_mask = os.path.join(tmp_output_dir, 'nan_mask.nii.gz')

        # ==========================================================================================================
        # create data for test
        # ==========================================================================================================
        amplitude = (1000 + 10 * randn(self.n_spatial_samples, self.n_spatial_samples,
                                       self.n_spatial_samples)) * np.exp(1j * np.random.uniform(0, 2 * np.pi, (
        self.n_spatial_samples, self.n_spatial_samples, self.n_spatial_samples)))  # don't care about units
        A = amplitude.real
        B = amplitude.imag
        TE = np.linspace(.005, .02, self.n_time_samples)  # s
        freq = 450 + 10 * randn(self.n_spatial_samples, self.n_spatial_samples,
                                self.n_spatial_samples)  # between 400 and 500 rad/s
        R2star = 50 + 10 * randn(self.n_spatial_samples, self.n_spatial_samples, self.n_spatial_samples)  # s-1
        self.R2star_data = R2star

        input_data = np.empty(
            (self.n_spatial_samples, self.n_spatial_samples, self.n_spatial_samples, self.n_time_samples),
            dtype='complex')
        for i in range(self.n_spatial_samples):
            for j in range(self.n_spatial_samples):
                for k in range(self.n_spatial_samples):
                    input_data[i, j, k, :] = model_monoexponential_R2Star(TE, A[i, j, k], B[i, j, k], R2star[i, j, k],
                                                                          freq[i, j, k])

        mag_data = np.abs(input_data)
        phase_data = np.angle(input_data)

        for n in range(self.n_time_samples):
            jsonloc = self.json_file[n]
            with open(jsonloc, 'w') as f:
                json.dump({"EchoTime": TE[n]}, f)

            img = nib.Nifti1Image(mag_data[..., n], np.eye(4))
            nib.save(img, self.mag[n])

            img = nib.Nifti1Image(phase_data[..., n], np.eye(4))
            nib.save(img, self.phase[n])

        img = nib.Nifti1Image(freq, np.eye(4))
        nib.save(img, self.freq_loc)
        img = nib.Nifti1Image(np.ones_like(freq, dtype='int'), np.eye(4))
        nib.save(img, self.mask)

    def test_calculation(self):
        test = pe.Node(interface=CalcR2Star(), name="test_CalcR2Star")  # problem with requests version
        test.inputs.mag = self.mag
        test.inputs.phase = self.phase
        test.inputs.json = self.json_file
        test.inputs.freq_loc = self.freq_loc
        test.inputs.mask = self.mask

        test.inputs.R2star = self.R2star
        test.inputs.neg_mask = self.neg_mask
        test.inputs.nan_mask = self.nan_mask

        test.run()

        R2star_estimated = nib.load(test.inputs.R2star).get_data()

        # test calc
        npt.assert_almost_equal(self.R2star_data, R2star_estimated)

    def tearDown(self):
        # unnecessary when using tempfile.TemporaryDirectory
        if type(self.mag) == list:
            for img in self.mag:
                os.remove(img)
        if type(self.phase) == list:
            for img in self.phase:
                os.remove(img)
        if type(self.json_file) == list:
            for f in self.json_file:
                os.remove(f)
        os.remove(self.freq_loc)
        os.remove(self.mask)
        os.remove(self.R2star)
        # ===========================================================
        # goodness of fit file
        abs_path = self.R2star
        foldername = os.path.dirname(abs_path)
        filename = os.path.basename(abs_path)
        ext, name = os.path.splitext(filename[::-1])
        name = name[:0:-1]
        ext = '.' + ext[::-1]
        full_name = os.path.join(foldername, (name + '_fit' + ext))
        os.remove(os.path.abspath(full_name))
        # ===========================================================
        os.remove(self.neg_mask)
        os.remove(self.nan_mask)


if __name__ == '__main__':
    unittest.main()
