import nipype.pipeline.engine as pe
import os
import unittest
from cfmm_nipype_interfaces.qsm import MEDI
from cfmm_nipype_interfaces import MatlabRunMode
from cfmm_nipype_interfaces.numerical_phantoms.qsmphantom import create_phantom, phantom_type
import numpy as np
import nibabel as nib
import tempfile


class TestMedi(unittest.TestCase):
    def __init__(self, *args, run_mode=MatlabRunMode.CONTAINER_MCR, **kwargs):
        self.run_mode = run_mode
        super(TestMedi, self).__init__(*args, **kwargs)

    def setUp(self):
        tmpdir_obj = tempfile.TemporaryDirectory()
        self.tmpdir_obj = tmpdir_obj
        self.tmpdir = tmpdir_obj.name
        dim_sz = 100
        FOV_mm = 320
        complex_data, ellipsoids, susc_values_ppb, _, freq_local, _, _ = create_phantom(dim_sz, FOV_mm,
                                                                                        ptype=phantom_type.COMPLEX)
        main_ellipsoid = ellipsoids[..., 0]
        self.fieldmap = os.path.join(self.tmpdir, 'freq.nii.gz')
        affine = np.eye(4) * np.diag([FOV_mm / dim_sz, FOV_mm / dim_sz, FOV_mm / dim_sz, 1])
        img = nib.Nifti1Image(freq_local, affine)
        img.header.set_xyzt_units('mm')
        nib.save(img, self.fieldmap)
        self.mag = os.path.join(self.tmpdir, 'mag.nii.gz')
        img = nib.Nifti1Image(np.abs(complex_data).sum(axis=-1), affine)
        img.header.set_xyzt_units('mm')
        nib.save(img, self.mag)
        self.mask = os.path.join(self.tmpdir, 'mask.nii.gz')
        img = nib.Nifti1Image(main_ellipsoid.astype('int8'), affine)
        img.header.set_xyzt_units('mm')
        nib.save(img, self.mask)
        self.rois = ellipsoids
        self.susc_values_ppb = susc_values_ppb

    def test_calculation(self):
        matlab_executable = '/usr/local/matlab/R2016b/bin/matlab'
        local_mcr_location = '/storage/akuurstr/MATLAB_R2016b/v91/'

        medi_node = pe.Node(interface=MEDI(run_mode=self.run_mode, matlab_executable=matlab_executable,
                                           mcr_loc=local_mcr_location), name="test_MEDI")
        medi_node.inputs.freq_loc = self.fieldmap
        medi_node.inputs.mag_loc = self.mag
        medi_node.inputs.mask_loc = self.mask
        # medi_node.inputs.reliability_mask_loc =
        # medi_node.inputs.gradient_mask_loc =
        medi_node.inputs.CF = 298060001.0
        medi_node.inputs.alpha = 1000.0
        # medi_node.inputs.merit = False
        medi_node.inputs.B0_dir = 3
        medi_node.inputs.susceptibility_filename = os.path.join(self.tmpdir, 'susceptibility_map.nii.gz')

        medi_node_result = medi_node.run()
        img_loc = medi_node.inputs.susceptibility_filename
        measured = nib.load(img_loc).get_data()

        for roi_index in range(1, self.rois.shape[-1]):
            susc_value_measured = measured[self.rois[..., roi_index].astype('bool')].mean()
            susc_value_true = self.susc_values_ppb[0] + self.susc_values_ppb[roi_index]
            self.assertLessEqual(np.abs((susc_value_measured - susc_value_true) / susc_value_true), 0.05,
                                 "ROI %s susceptibility failed. Measured: %s, Actual: %s" % (
                                     roi_index, susc_value_measured, susc_value_true))


if __name__ == '__main__':
    # make sure you run this file and not pycharm's "unittests for ..."  or "unittests in ..."
    suite = unittest.TestSuite()
    # suite.addTest(TestMedi('test_calculation', run_mode=MatlabRunMode.LOCAL_MATLAB))
    suite.addTest(TestMedi('test_calculation', run_mode=MatlabRunMode.LOCAL_MCR))
    runner = unittest.TextTestRunner()
    runner.run(suite)
