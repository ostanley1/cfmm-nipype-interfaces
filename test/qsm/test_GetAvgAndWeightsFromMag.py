import nipype.pipeline.engine as pe
import os
import unittest
from cfmm_nipype_interfaces.qsm import GetAvgAndSNRMap
import nibabel as nib
import numpy as np
import numpy.testing as npt
import tempfile


class TestGetAvgAndWeightsFromMag(unittest.TestCase):
    def setUp(self):
        self.tmp_generated_images_dir_obj = tempfile.TemporaryDirectory()
        tmp_generated_images_dir = self.tmp_generated_images_dir_obj.name
        self.tmp_output_dir_obj = tempfile.TemporaryDirectory()
        self.tmp_output_dir = self.tmp_output_dir_obj.name

        self.img_locs = [os.path.join(tmp_generated_images_dir, 'simulated_mag1.nii.gz'),
                         os.path.join(tmp_generated_images_dir, 'simulated_mag2.nii.gz')]
        self.imgs = [np.random.uniform(0, 1000, (10, 10, 10)), np.random.uniform(0, 1000, (10, 10, 10))]
        for img, loc in zip(self.imgs, self.img_locs):
            imgobj = nib.Nifti1Image(img, np.eye(4))
            nib.save(imgobj, loc)

    def test(self):
        test_node = pe.Node(interface=GetAvgAndSNRMap(), name="test_GetAvgAndSNRMap")
        test_node.inputs.mag = self.img_locs
        test_node.inputs.snr_window_sz = 3
        test_node.inputs.avg_out_filename = os.path.join(self.tmp_output_dir, 'mag_avg.nii.gz')
        test_node.inputs.snr_map_out_filename = os.path.join(self.tmp_output_dir, 'snr_weight.nii.gz')

        test_node.run()

        self.test_node = test_node

        # basic
        self.assertTrue(nib.load(test_node.inputs.avg_out_filename))

        # test snr map
        snr_map = nib.load(test_node.inputs.snr_map_out_filename).get_data()
        test_snr_value = snr_map[1, 1, 1, 0]
        original_img = nib.load(self.img_locs[0]).get_data()
        original_img_window = original_img[:3, :3, :3]
        true_snr_value = original_img_window.mean() / original_img_window.var()
        self.assertAlmostEqual(test_snr_value, true_snr_value)

        # test weighted avg
        avg = nib.load(test_node.inputs.avg_out_filename).get_data()

        original_imgs = []
        for loc in self.img_locs:
            original_imgs.append(nib.load(loc).get_data())
        to_avg = np.asarray(original_imgs).transpose(1, 2, 3, 0)
        weights = np.abs(to_avg).mean(axis=(0, 1, 2))
        weights = weights / weights.sum()  # normalized
        true_avg = (to_avg * weights).sum(axis=-1)
        npt.assert_almost_equal(avg, true_avg)

    def tearDown(self):
        # unnecessary when using tempfile.TemporaryDirectory
        for loc in self.img_locs:
            os.remove(loc)
        os.remove(self.test_node.inputs.avg_out_filename)
        os.remove(self.test_node.inputs.snr_map_out_filename)


if __name__ == '__main__':
    unittest.main()
