import nipype.pipeline.engine as pe
import os
import unittest
from cfmm_nipype_interfaces.phase import EstimateFrequencyFromWrappedPhase
from cfmm_nipype_interfaces.numerical_phantoms.qsmphantom import create_phantom, phantom_type
import numpy as np
import nibabel as nib
import tempfile
import glob
import numpy.testing as npt


class TestGetParamFromJson(unittest.TestCase):
    def setUp(self):
        tmpdir_obj = tempfile.TemporaryDirectory()
        self.tmpdir_obj = tmpdir_obj
        self.tmpdir = tmpdir_obj.name
        self.subj_dir, ellipsoids, _, _, _, self.fieldmap = create_phantom(ptype=phantom_type.BIDS,
                                                                           bids_dir=self.tmpdir, siemens_phase=False)
        main_ellipsoid = ellipsoids[..., 0]
        self.fieldmap = self.fieldmap * main_ellipsoid
        self.mask = os.path.join(self.tmpdir, 'mask.nii.gz')
        img = nib.Nifti1Image(main_ellipsoid.astype('int8'), np.eye(4))
        nib.save(img, self.mask)
        self.mag_locs = glob.glob(os.path.join(self.subj_dir, '*mag*.nii*'))
        self.mag_locs.sort()
        self.phase_locs = glob.glob(os.path.join(self.subj_dir, '*phase*.nii*'))
        self.phase_locs.sort()
        self.phase_json_locs = glob.glob(os.path.join(self.subj_dir, '*phase*.json'))
        self.phase_json_locs.sort()

    def test(self):
        freq_est_node = pe.Node(interface=EstimateFrequencyFromWrappedPhase(),
                                name="test_EstimateFrequencyFromWrappedPhase")
        freq_est_node.inputs.phase = self.phase_locs
        freq_est_node.inputs.json = self.phase_json_locs
        freq_est_node.inputs.mask = self.mask
        # just test the ideal noiseless case for now
        # freq_est_node.inputs.weight =
        feq_est_filename = os.path.join(self.tmpdir, 'freq_est.nii.gz')
        freq_est_node.inputs.freq_filename = feq_est_filename

        freq_est_node_result = freq_est_node.run()
        freq_est_measured = nib.load(feq_est_filename).get_data()
        npt.assert_almost_equal(self.fieldmap, freq_est_measured)

    def test_noise(self):
        tmpdir_obj = tempfile.TemporaryDirectory()
        self.tmpdir_obj = tmpdir_obj
        self.tmpdir = tmpdir_obj.name
        self.subj_dir, ellipsoids, _, _, _, self.fieldmap = create_phantom(ptype=phantom_type.BIDS,
                                                                           bids_dir=self.tmpdir, siemens_phase=False,
                                                                           snr=100)
        main_ellipsoid = ellipsoids[..., 0]
        self.fieldmap = self.fieldmap * main_ellipsoid
        self.mask = os.path.join(self.tmpdir, 'mask.nii.gz')
        img = nib.Nifti1Image(main_ellipsoid.astype('int8'), np.eye(4))
        nib.save(img, self.mask)
        self.mag_locs = glob.glob(os.path.join(self.subj_dir, '*mag*.nii*'))
        self.mag_locs.sort()
        self.phase_locs = glob.glob(os.path.join(self.subj_dir, '*phase*.nii*'))
        self.phase_locs.sort()
        self.phase_json_locs = glob.glob(os.path.join(self.subj_dir, '*phase*.json'))
        self.phase_json_locs.sort()

        use_weights = True

        if not use_weights:
            freq_est_noisy_node = pe.Node(interface=EstimateFrequencyFromWrappedPhase(),
                                          name="test_EstimateFrequencyFromWrappedPhase_noisy")
            freq_est_noisy_node.inputs.phase = self.phase_locs
            freq_est_noisy_node.inputs.json = self.phase_json_locs
            freq_est_noisy_node.inputs.mask = self.mask
            # just test the ideal noiseless case for now
            # freq_est_noisy_node.inputs.weight =
            freq_est_noisy_filename = os.path.join(self.tmpdir, 'freq_est_noisy.nii.gz')
            freq_est_noisy_node.inputs.freq_filename = freq_est_noisy_filename

            freq_est_noisy_node_result = freq_est_noisy_node.run()
            freq_est_noisy_measured = nib.load(freq_est_noisy_filename).get_data()

            freq_est_noisy_weights_measured = freq_est_noisy_measured

        else:
            from cfmm_nipype_interfaces.qsm import GetAvgAndSNRMap
            snr_node = pe.Node(interface=GetAvgAndSNRMap(), name="test_GetAvgAndSNRMap")
            snr_node.inputs.mag = self.mag_locs
            snr_node.inputs.snr_window_sz = 10
            snr_node.inputs.avg_out_filename = os.path.join(self.tmpdir, 'mag_avg.nii.gz')
            snr_node.inputs.snr_map_out_filename = os.path.join(self.tmpdir, 'snr_weight.nii.gz')
            snr_node.run()
            # snr_img=nib.load(snr_node.inputs.snr_map_out_filename).get_data()

            freq_est_noisy_weights_node = pe.Node(interface=EstimateFrequencyFromWrappedPhase(),
                                                  name="test_EstimateFrequencyFromWrappedPhase_noisy_weights")
            freq_est_noisy_weights_node.inputs.phase = self.phase_locs
            freq_est_noisy_weights_node.inputs.json = self.phase_json_locs
            freq_est_noisy_weights_node.inputs.mask = self.mask
            freq_est_noisy_weights_node.inputs.weight = snr_node.inputs.snr_map_out_filename
            freq_est_noisy_weights_filename = os.path.join(self.tmpdir, 'freq_est_noisy_weights.nii.gz')
            freq_est_noisy_weights_node.inputs.freq_filename = freq_est_noisy_weights_filename
            freq_est_noisy_weights_node_result = freq_est_noisy_weights_node.run()
            freq_est_noisy_weights_measured = nib.load(freq_est_noisy_weights_filename).get_data()

        error_percent = np.abs(self.fieldmap - freq_est_noisy_weights_measured) / (
                    self.fieldmap + np.finfo(float).eps) * 100
        self.assertTrue((error_percent < 15).all())


if __name__ == '__main__':
    unittest.main()
