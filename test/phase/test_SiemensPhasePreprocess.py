import nipype.pipeline.engine as pe
import os
import unittest
from cfmm_nipype_interfaces.phase import SiemensPhasePreprocess
import numpy as np
import nibabel as nib
import numpy.testing as npt
import tempfile

class TestSiemensPhasePreprocess(unittest.TestCase):
    def setUp(self):
        self.tmp_generated_images_dir_obj = tempfile.TemporaryDirectory()
        tmp_generated_images_dir = self.tmp_generated_images_dir_obj.name
        self.tmp_output_dir_obj = tempfile.TemporaryDirectory()
        self.tmp_output_dir = self.tmp_output_dir_obj.name

        self.img_loc = os.path.join(tmp_generated_images_dir, 'simulated_phase_image.nii.gz')
        self.img = np.random.randint(0,2048, (10,10,10))
        imgobj = nib.Nifti1Image(self.img, np.eye(4))
        nib.save(imgobj, self.img_loc)

    def test(self):
        # ==========================================================================================================
        # run node on created data
        # ==========================================================================================================
        test_node = pe.Node(interface=SiemensPhasePreprocess(), name="test_SiemensPhasePreprocess")
        test_node.inputs.infiles = [self.img_loc,]
        test_node.inputs.output_folder = self.tmp_output_dir
        test_node_result = test_node.run()
        self.node_output = test_node_result.outputs.outfiles

        node_result = nib.load(self.node_output).get_data()

        npt.assert_equal((self.img/ 2048 - 1) * np.pi, node_result)

    def tearDown(self):
        # unnecessary when using tempfile.TemporaryDirectory
        os.remove(self.img_loc)
        os.remove(self.node_output)

if __name__=='__main__':
    unittest.main()