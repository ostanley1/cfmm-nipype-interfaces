import nipype.pipeline.engine as pe
import os
import unittest
from cfmm_nipype_interfaces.text_files import GetParamFromJson, GetCFFromJson


class TestGetParamFromJson(unittest.TestCase):
    def setUp(self):
        self.json_file = os.path.join(os.path.dirname(__file__), 'sub-C001_part-phase_echo-1_GRE.json')

    def test(self):
        test = pe.Node(interface=GetParamFromJson, name="test_GetParamFromJson")
        test.inputs.filename = self.json_file
        test.inputs.parameter = "ImagingFrequency"
        result = test.run()
        self.assertEqual(result.outputs.parameter_value, 297.22987)

        test = pe.Node(interface=GetCFFromJson, name="test_GetCFFromJson")
        test.inputs.filename = self.json_file
        result = test.run()
        self.assertEqual(result.outputs.CF_value, 297229870.0)


if __name__ == '__main__':
    unittest.main()
