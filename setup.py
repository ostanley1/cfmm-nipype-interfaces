from setuptools import setup, find_namespace_packages
setup(
    name="cfmm_nipype_interfaces",
    version="0.1",
    install_requires=[
        'nipype==1.3.0rc1',
        'numpy==1.16.2',
        'scipy==1.2.1',
        'nibabel==2.4.0',
        # requests update needed for nipype 1.3.0rc1 to work correctly
        'requests==2.22.0',
        'pyunwrap3d @ git+https://@github.com/AlanKuurstra/pyunwrap3d.git#egg=pyunwrap3d',
        'pyqsm @ git+https://@github.com/AlanKuurstra/pyqsm.git@python3#egg=pyqsm',
    ],
    packages=find_namespace_packages(),
    # this copies the matlab mcr scripts
    package_data={'': ['*matlabscript*']},
    include_package_data=True,
)